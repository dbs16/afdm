import { faker } from "@faker-js/faker";
import * as fs from "fs";

const PEOPLE = [];
const SQL = [
  `BEGIN TRANSACTION;`,
  `DELETE FROM roll_of_honour;`,
  `INSERT INTO roll_of_honour (pk_id, surname, forenames, decorations, rank, service_no, regt_corps, service, death_date, age) VALUES `,

];

const VALUES = [];

for (let i = 0; i < 1000; i++) {
  PEOPLE.push({
    pk_id: faker.datatype.uuid(),
    surname: faker.name.lastName(),
    forenames: faker.name.firstName(),
    decorations: faker.random.words(),
    rank: faker.name.jobType(),
    service_no: faker.random.numeric(6),
    regt_corps: faker.random.words(),
    service: ["Army", "Navy", "Marines", "RAF"][faker.datatype.number(3)],
    date_of_death: faker.date.birthdate(),
    age: faker.datatype.number({min: 16, max: 100}),
  });

  VALUES.push(
    `( '${PEOPLE[i].pk_id}', '${PEOPLE[i].surname.replace("'", "''")}', '${PEOPLE[i].forenames}', '${PEOPLE[i].decorations}', '${PEOPLE[i].rank}', '${PEOPLE[i].service_no}', '${PEOPLE[i].regt_corps}', '${PEOPLE[i].service}', '${PEOPLE[i].date_of_death}', ${PEOPLE[i].age})`
  )

  if(i % 100 === 0) {
    console.log(`Generated ${i} people`);
    SQL.push(VALUES.join(',\r'));
    VALUES.length = 0;

    SQL.push(';');
    // SQL.push(`;COMMIT;`);
    // SQL.push(`BEGIN TRANSACTION;`);
    SQL.push('INSERT INTO roll_of_honour (pk_id, surname, forenames, decorations, rank, service_no, regt_corps, service, death_date, age) VALUES \r')
  }
}

SQL.push(VALUES.join(',\r'));
SQL.push(`;COMMIT;`);

fs.createWriteStream("people.json").write(JSON.stringify(PEOPLE));
fs.createWriteStream("people.sql").write(SQL.join("\r"));
