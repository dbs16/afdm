import * as fs from "fs";
import { parse } from "csv-parse";
import { v4 as uuidv4 } from "uuid";
import sqlite3 from "sqlite3";
import { TransactionDatabase } from "sqlite3-transactions";

const csvData = [];
const csvMap = {
  pk_id: 0,
  serial_no: 1,
  surname: 2,
  forenames: 3,
  gender: 4,
  decorations: 5,
  rank: 6,
  service_no: 7,
  regt_corps: 8,
  birth_date: 9,
  service: 10,
  memorial: 11,
  nom_roll: 12,
  age: 13,
  death_date: 14,
  cemetery_name: 15,
  cemetery_address_1: 16,
  cemetery_address_2: 17,
  cemetery_address_3: 18,
  cemetery_address_4: 19,
  cemetery_postcode: 20,
  grave_section: 21,
  grave_row: 22,
  grave_number: 23
};
const csvKeys = Object.keys(csvMap);
const rows = [];

fs.createReadStream("data.csv")
  .pipe(parse({
    delimiter: ",", from_line: 2, relax_column_count: false,
    skip_empty_lines: true, trim: false, skip_lines_with_error: true,
    relax: true, relax_column_count_more: true
  }))
  .on("data", function(csvrow) {
    const pkID = uuidv4() ?? "SomeUUID";

    for (let key of csvKeys) {
      if (csvrow[csvMap[key]] == undefined) {
        console.log("Key undefined", key, csvMap[key], csvrow);
        process.exit(1);
      }
    }

    rows.push(`INSERT INTO roll_of_honour (serial_no, surname, forenames, gender, decorations, rank, service_no,
                                           regt_corps, birth_date, service, memorial, nom_roll, age, death_date,
                                           cemetery_name, cemetery_address_1, cemetery_address_2, cemetery_address_3,
                                           cemetery_address_4, cemetery_postcode, grave_section, grave_row,
                                           grave_number)
               VALUES ("${csvrow[csvMap.serial_no].replace("\"", "\"\"")}",
                       "${csvrow[csvMap.surname].replace("\"", "\"\"")}",
                       "${csvrow[csvMap.forenames].replace("\"", "\"\"")}",
                       "${csvrow[csvMap.gender].replace("\"", "\"\"")}",
                       "${csvrow[csvMap.decorations].replace("\"", "\"\"")}",
                       "${csvrow[csvMap.rank].replace("\"", "\"\"")}",
                       "${csvrow[csvMap.service_no].replace("\"", "\"\"")}",
                       "${csvrow[csvMap.regt_corps].replace("\"", "\"\"")}",
                       "${csvrow[csvMap.birth_date].replace("\"", "\"\"")}",
                       "${csvrow[csvMap.service].replace("\"", "\"\"")}",
                       "${csvrow[csvMap.memorial].replace("\"", "\"\"")}",
                       "${csvrow[csvMap.nom_roll].replace("\"", "\"\"")}",
                       "${csvrow[csvMap.age].replace("\"", "\"\"")}",
                       "${csvrow[csvMap.death_date].replace("\"", "\"\"")}",
                       "${csvrow[csvMap.cemetery_name].replace("\"", "\"\"")}",
                       "${csvrow[csvMap.cemetery_address_1].replace("\"", "\"\"")}",
                       "${csvrow[csvMap.cemetery_address_2].replace("\"", "\"\"")}",
                       "${csvrow[csvMap.cemetery_address_3].replace("\"", "\"\"")}",
                       "${csvrow[csvMap.cemetery_address_4].replace("\"", "\"\"")}",
                       "${csvrow[csvMap.cemetery_postcode].replace("\"", "\"\"")}",
                       "${csvrow[csvMap.grave_section].replace("\"", "\"\"")}",
                       "${csvrow[csvMap.grave_row].replace("\"", "\"\"")}",
                       "${csvrow[csvMap.grave_number].replace("\"", "\"\"")}")`);
  })
  .on("end", function() {
    const db = connectToDatabase();
    db.beginTransaction((err, transaction) => {
      rows.forEach(row => {
        try {
          // console.log(row);
          transaction.run(row);
        } catch (e) {
          console.log(e);
        }
      });

      transaction.commit((err, result) => {
        if (err) {
          console.log("Error committing transaction", err, row);
        }
        console.log("transaction committed");
      });
    });
  })
  .on("error", function(err) {
    console.log(err);
  });


function connectToDatabase() {
  if (fs.existsSync("database.db")) {
    return new TransactionDatabase(new sqlite3.Database("database.db", sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE));
  }

  return db;
}

