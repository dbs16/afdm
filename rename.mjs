import { faker } from "@faker-js/faker";
import sqlite3 from "sqlite3";

// Fetch the forenames and surnames from ./afmd.db SQLIte database
const db = new sqlite3.Database("afmd.db");
const query = "UPDATE roll_of_honour SET forenames = ?, surname = ? WHERE id = ?";

db.all("SELECT id, forenames, surname, gender FROM roll_of_honour", (err, rows) => {
  let forename = "";
  let surname = "";


  rows.forEach(row => {
    forename = faker.name.firstName(row.gender === "Male" ? "male" : "female");
    surname = faker.name.lastName();
    markername = forename + " " + surname;

    db.run(query, [forename, surname, row.id], (err, result) => {
      if (err) {
        console.log(err);
      }
    });
    // db.update("UPDATE roll_of_honour SET forenames = ?, surname = ? WHERE id = ?", [
    //   forename,
    //   surname,
    //   row.id
    // ]);
  });
});