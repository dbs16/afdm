import { Inject, Injectable } from "@nestjs/common";
import { RollOfHonour } from "./roll_of_honour.entity";
import { Repository } from "typeorm";
import { paginate, Pagination } from "nestjs-typeorm-paginate";
import * as fs from "fs";

@Injectable()
export class RollOfHonourService {

  constructor(@Inject("ROLL_OF_HONOUR_REPOSITORY") private repository: Repository<RollOfHonour>) {
  }

  async findAll(): Promise<RollOfHonour[]> {
    return await this.repository.find();
  }

  async search({
                 page, limit, route,
                 service,
                 surname,
                 forenames,
                 service_number
               }): Promise<Pagination<RollOfHonour>> {
    const queryBuilder = this.repository.createQueryBuilder("RollOfHonour")
      .orderBy("surname", "ASC")
      .addOrderBy("forenames", "ASC");

    if (forenames) {
      queryBuilder.andWhere(`LOWER(RollOfHonour.forenames) LIKE LOWER(:forenames)`, { forenames: `%${forenames}%` });
    }

    if (surname) {
      queryBuilder.andWhere(`LOWER(RollOfHonour.surname) LIKE LOWER(:surname)`, { surname: `%${surname}%` });
    }

    if (service_number.length > 0) {
      service_number = service_number.replace(/\s/g, "").toString();
      queryBuilder.andWhere("RollOfHonour.service_no LIKE (:service_number)", { service_number: `%${service_number}%` });
    }

    if (service != "any") {
      queryBuilder.andWhere("RollOfHonour.service IN (:...service)", { service: service });
    }

    return paginate<RollOfHonour>(queryBuilder, { page, limit, route });
  }

  async get(id): Promise<RollOfHonour> {
    return await this.repository.findOneBy({ id: id });
  }

  // @Cron("59 * * * * *")
  checkImported() {
    if (!fs.existsSync("imported.txt")) {
      fs.writeFileSync("imported.txt", "imported");
      console.log("Importing Roll of Honour ", new Date());

      /** Delete file */
      fs.unlinkSync("imported.txt");
    }
  }
}
