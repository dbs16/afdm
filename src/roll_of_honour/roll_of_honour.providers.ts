import { DataSource } from "typeorm";
import { RollOfHonour } from "./roll_of_honour.entity";

export const rollOfHonourProviders = [
  {
    provide: "ROLL_OF_HONOUR_REPOSITORY",
    useFactory: (dataSource: DataSource) => dataSource.getRepository(RollOfHonour),
    inject: ["DATA_SOURCE"]
  }
];
