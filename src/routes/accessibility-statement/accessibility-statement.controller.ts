import { Controller, Get, Render } from "@nestjs/common";

@Controller('accessibility-statement')
export class AccessibilityStatementController {

  @Get("/")
  @Render("accessibility-statement/index")
  index() {
    return {};
  }

}
