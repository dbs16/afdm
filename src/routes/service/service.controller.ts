import { Controller, Get, Post, Render, Req, Res } from "@nestjs/common";

@Controller("service")
export class ServiceController {

  @Get()
  @Render("service/index")
  async service(@Req() req, @Res() res) {
    const errors = await req.consumeFlash("errors");
    const inline = [];

    if (errors) {
      for (let error of errors) {
        inline[error.href.substring(1)] = { text: error.text };
      }
    }

    return {
      errors: errors,
      inline: inline,
      service: req.body.service ?? req.session["service"] ?? null
    };
  }

  @Post()
  async storeService(@Req() req, @Res() res) {
    const fields = ["service"];
    const post = req.body;

    if (!post.hasOwnProperty("service")) {
      await req.flash("errors", {
        text: "Select at least one service",
        href: "#service"
      });

      return res.redirect("/");
    }

    for (let field of fields) {
      if (post.hasOwnProperty(field)) {
        req.session[field] = post[field];
      }
    }

    req.session.save(function() {
      res.redirect("/details");
    });
  }

}
