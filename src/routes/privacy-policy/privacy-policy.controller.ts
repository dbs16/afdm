import { Controller, Get, Render } from "@nestjs/common";

@Controller("privacy-policy")
export class PrivacyPolicyController {

  @Get("/")
  @Render("privacy-policy/index")
  index() {
    return {};
  }
  
}
