import { Controller, Get, Post, Render, Req, Res } from "@nestjs/common";
import { FeedbackService } from "./feedback.service";

@Controller("feedback")
export class FeedbackController {
  constructor(private readonly feedbackService: FeedbackService) {
  }

  @Get()
  @Render("feedback/index")
  index() {
    return {};
  }

  @Post()
  send(@Req() req: any, @Res() res: any) {
    const bodyParams = req.body;
    this.feedbackService.send({
      satisfaction: bodyParams.satisfaction,
      feedback: bodyParams.feedback,
      name: bodyParams.name,
      email: bodyParams.email
    });
    return res.redirect("/feedback/sent");
  }

  @Get("/sent")
  @Render("feedback/sent")
  sent() {
    return {};
  }

}
