import { Injectable } from "@nestjs/common";


@Injectable()
export class FeedbackService {
  send({ satisfaction, feedback, name, email }) {
    let NotifyClient = require("notifications-node-client").NotifyClient;
    let notifyClient = new NotifyClient("rollofhonour-8ae4b688-c5e2-45ff-a873-eb149b3e23ff-920a9922-c710-44b4-a901-38515f001a7c");
    let templateId = "0f3b68c3-4589-4466-a743-73f73e841187";
    let personalisation = {
      "service": satisfaction || "Not provided",
      "feedback": feedback || "No feedback provided",
      "name": name || "No feedback provided",
      "email": email || "No feedback provided"
    };

    notifyClient.sendEmail(templateId, process.env.GOV_NOTIFY_EMAIL, {
      personalisation: personalisation
    })
      .then(response => {
      })
      .catch(err => console.error(err, err.response, err.response.data));
  }
}
