import { Body, Controller, Get, Post, Render, Req, Res } from "@nestjs/common";

@Controller("cookie-policy")
export class CookiePolicyController {
  @Get()
  @Render("cookie-policy/index")
  async index(@Req() req, @Res() res) {
    const cookies = req.cookies;
    const cookiePreferences = JSON.parse(cookies.cookie_preferences ?? "{\"analytics\": false, \"seen\": false, \"essential\": true}");
    cookiePreferences.seen = true;
    res.cookie("cookie_preferences", JSON.stringify(cookiePreferences));

    const showNotification = await req.consumeFlash("saved");

    return {
      cookieUsagePreference: cookiePreferences.analytics ? "use" : "dont-use",
      showNotification: showNotification[0] ?? false,
      showCookieBanner: false,
      usage: [
        { name: "_ga", purpose: "Used to distinguish users", expires: "2 years" },
        { name: "_ga_YEF99LLDKP", purpose: "Used to distinguish users", expires: "2 years" }
      ],
      essential: [
        {
          name: "csrftoken",
          purpose: "A standard cookie used to prevent a malicious exploit of a website",
          expires: "2 hours"
        },
        {
          name: "connect.sid",
          purpose: "Holds session data to complete the application",
          expires: "When you close your browser"
        },
        {
          name: "cookies_preference",
          purpose: "Registers the input cookie preference",
          expires: "When you close your browser"
        }
      ],
      settings: [],
      campaigns: []
    };
  }

  @Post()
  async save(@Req() req, @Res() res, @Body() body) {
    const backURL = req.header("Referer") || "/";
    const cookies = req.cookies;
    const cookiePreferences = JSON.parse(cookies.cookie_preferences);

    cookiePreferences.analytics = body["cookie-usage-preference"] === "use";
    cookiePreferences.seen = true;

    await req.flash("saved", true);

    res.cookie("cookie_preferences", JSON.stringify(cookiePreferences));
    res.redirect(backURL);
  }
}
