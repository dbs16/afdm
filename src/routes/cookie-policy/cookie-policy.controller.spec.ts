import { Test, TestingModule } from '@nestjs/testing';
import { CookiePolicyController } from './cookie-policy.controller';

describe('CookiePolicyController', () => {
  let controller: CookiePolicyController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CookiePolicyController],
    }).compile();

    controller = module.get<CookiePolicyController>(CookiePolicyController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
