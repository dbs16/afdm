import { Test, TestingModule } from '@nestjs/testing';
import { CookiePolicyService } from './cookie-policy.service';

describe('CookiePolicyService', () => {
  let service: CookiePolicyService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CookiePolicyService],
    }).compile();

    service = module.get<CookiePolicyService>(CookiePolicyService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
