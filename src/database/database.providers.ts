import { DataSource } from "typeorm";

const VCAP_SERVICES = JSON.parse(process.env.VCAP_SERVICES || "{}");
export const databaseProviders = [
  {
    provide: "DATA_SOURCE",
    useFactory: async () => {
      const dataSource = new DataSource({
        type: "postgres",
        host: process.env.NODE_ENV === "production" ? VCAP_SERVICES.postgres[0].credentials.host : "localhost",
        port: process.env.NODE_ENV === "production" ? VCAP_SERVICES.postgres[0].credentials.port : 5432,
        username: process.env.NODE_ENV === "production" ? VCAP_SERVICES.postgres[0].credentials.username : "postgres",
        password: process.env.NODE_ENV === "production" ? VCAP_SERVICES.postgres[0].credentials.password : "example",
        database: process.env.NODE_ENV === "production" ? VCAP_SERVICES.postgres[0].credentials.name : "afmd",
        ssl: process.env.NODE_ENV === "production" ? {
          rejectUnauthorized: false,
          ca: "/etc/cf-system-certificates/trusted-ca-2.crt" // process.env.SSL_CERT
        } : false,
        // ssl: "Amazon RDS",
        entities: [__dirname + "/../**/*.entity{.ts,.js}"],
        synchronize: true,
        dropSchema: false
      });

      return dataSource.initialize();
    }
  }
];
