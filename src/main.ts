import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import * as nunjucks from "nunjucks";
import { NestExpressApplication } from "@nestjs/platform-express";
import * as session from "express-session";
import * as cookieParser from "cookie-parser";
import { join } from "path";
import { pagination } from "typeorm-pagination";
import { NotFoundExceptionFilter } from "./filters/not-found-exception.filter";
import { BadRequestExceptionFilter } from "./filters/bad-request-exception.filter";

const { flash } = require("express-flash-message");

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  const express = app.getHttpAdapter().getInstance();
  const assets = join(__dirname, "..", "public");
  const views = join(__dirname, "..", "views");
  const cookieSession = require("cookie-session");

  let RedisStore = require("connect-redis")(session);
  const { createClient } = require("redis");
  let redisClient;
  if (process.env.NODE_ENV === "production") {
    redisClient = createClient({
      legacyMode: true,
      url: JSON.parse(process.env.VCAP_SERVICES).redis[0].credentials.uri
    });
    redisClient.connect().catch(console.error);
  }

  nunjucks.configure([join(__dirname, "..", "node_modules", "govuk-frontend"), views], { express });

  app.useStaticAssets(assets);
  app.setBaseViewsDir(views);
  app.setViewEngine("njk");
  let env = new nunjucks.Environment();

  app.use(
    session({
      store: (process.env.ENVIRONMENT == "production") ? new RedisStore({ client: redisClient, ttl: 86400 }) : null,
      secret: process.env.SESSION_SECRET ?? "secret",
      resave: false,
      saveUninitialized: true,
      cookie: {
        secure: false,
        httpOnly: false,
        maxAge: 86400
      }
    })
  );
  app.use(flash({ sessionKeyName: "flashMessage" }));
  app.use(cookieParser());
  app.use(pagination);
  app.useGlobalFilters(new NotFoundExceptionFilter(), new BadRequestExceptionFilter());
  // app.use(csurf());

  await app.listen(process.env.PORT || 1337);
}

bootstrap();
