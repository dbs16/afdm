import { Injectable } from "@nestjs/common";
import * as AWS from "aws-sdk";

@Injectable()
export class DocumentService {
  config = process.env.VCAP_SERVICES ? JSON.parse(process.env.VCAP_SERVICES) : {};
  credentials = this.config["aws-s3-bucket"]?.[0]?.credentials ?? {};
  params = { Bucket: this.credentials.bucket_name, Key: "afmroh/counter.json" };

  // Constructor
  constructor() {
    AWS.config.update({
      region: this.credentials.aws_region,
      accessKeyId: this.credentials.aws_access_key_id,
      secretAccessKey: this.credentials.aws_secret_access_key
    });
  }

  async incrementCounter() {
    let s3 = new AWS.S3({ apiVersion: "2006-03-01" });

    // Create a key with month and year
    const date = new Date();
    const month = date.toLocaleString("default", { month: "long" });
    const year = date.getFullYear();
    const dateStr = `${month} ${year}`;

    let data;
    try {
      data = await s3.getObject(this.params).promise();
    } catch (e) {
      data = { Body: "{}" };
    }

    const json = JSON.parse(data.Body.toString());
    json[dateStr] = json[dateStr] ? json[dateStr] + 1 : 1;

    const uploadParams = { ...this.params, Body: JSON.stringify(json) };
    const uploaded = await s3.upload(uploadParams).promise();

    console.log(uploaded);
  }

  async getCounter() {
    let s3 = new AWS.S3({ apiVersion: "2006-03-01" });

    let data;
    try {
      data = await s3.getObject(this.params).promise();
    } catch (e) {
      data = { Body: "{}" };
    }

    return JSON.parse(data.Body.toString());
  }
}
