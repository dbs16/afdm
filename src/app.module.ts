import { MiddlewareConsumer, Module } from "@nestjs/common";

import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { ConfigModule } from "@nestjs/config";
import { RollOfHonourModule } from "./roll_of_honour/roll_of_honour.module";
import { rollOfHonourProviders } from "./roll_of_honour/roll_of_honour.providers";
import { RollOfHonourService } from "./roll_of_honour/roll_of_honour.service";
import { DatabaseModule } from "./database/database.module";
import { ShowPropertyMiddleware } from "./middleware/show_property.middleware";
import { FeedbackController } from "./routes/feedback/feedback.controller";
import { FeedbackService } from "./routes/feedback/feedback.service";
import { CookiePolicyController } from "./routes/cookie-policy/cookie-policy.controller";
import { CookiePolicyService } from "./routes/cookie-policy/cookie-policy.service";
import { SearchController } from "./routes/search/search.controller";
import { DocumentController } from "./document/document.controller";
import { DocumentService } from "./document/document.service";
import { SearchService } from "./routes/search/search.service";
import { ScheduleModule } from "@nestjs/schedule";
import { ServiceController } from "./routes/service/service.controller";
import { DetailsController } from "./routes/details/details.controller";
import { PrivacyPolicyController } from "./routes/privacy-policy/privacy-policy.controller";
import { AccessibilityStatementController } from "./routes/accessibility-statement/accessibility-statement.controller";

@Module({
  imports: [
    ConfigModule.forRoot(),
    DatabaseModule,
    RollOfHonourModule,
    ScheduleModule.forRoot()
  ],
  controllers: [AppController, FeedbackController, CookiePolicyController, SearchController, DocumentController, ServiceController, DetailsController, PrivacyPolicyController, AccessibilityStatementController],
  providers: [AppService, ...rollOfHonourProviders, RollOfHonourService, FeedbackService, CookiePolicyService, DocumentService, SearchService]
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(ShowPropertyMiddleware)
      .forRoutes("/*");
  }
}