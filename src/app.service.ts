import { Injectable } from "@nestjs/common";

@Injectable()
export class AppService {


  /**
   * Get value from cookied called cookie_preferences
   */
  getCookiePreferences(req) {
    if (req.cookies.cookie_preferences) {
      return req.cookies.cookie_preferences;
    }

    return null;
  }

  getOutputForRollOfHonour(result, newLine = "\n") {
    let corpLabel = "Corps";

    switch (result.service) {
      case "Royal Fleet Auxiliary":
      case "Royal Navy":
        corpLabel = "Ship / Unit";
        break;
      case "Royal Air Force":
        corpLabel = "Squadron";
        break;
      case "Army":
        corpLabel = "Regiment";
        break;
      case "Merchant Navy":
        corpLabel = "Regiment";
        break;
    }

    const cemetery_address = result.cemetery_address_1 ? [
      result.cemetery_address_1 ?? null,
      result.cemetery_address_2 ?? null,
      result.cemetery_address_3 ?? null,
      result.cemetery_address_4 ?? null
    ].join(newLine ?? "<br>") : null;

    const summary = [
      result.surname ? { key: { text: "Last name" }, value: { text: result.surname } } : null,
      {
        key: { text: "First name(s) / Initial(s)" }, value: {
          text: result.forenames.toLowerCase()
            .split(" ")
            .map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(" ")
            .split("-")
            .map(word => word.charAt(0).toUpperCase() + word.slice(1)).join("-")

        }
      },
      result.rank ? { key: { text: "Rank" }, value: { text: result.rank } } : null,
      result.regt_corps ? { key: { text: corpLabel }, value: { text: result.regt_corps } } : null,
      result.service ? { key: { text: "Service" }, value: { text: result.service } } : null,
      result.service_no ? { key: { text: "Service Number" }, value: { text: result.service_no } } : null,
      result.decorations ? { key: { text: "Decorations" }, value: { text: result.decorations } } : null,
      result.birth_date ? { key: { text: "Date of Birth" }, value: { text: result.birth_date } } : null,
      result.age ? { key: { text: "Age" }, value: { text: result.age } } : null,
      result.death_date ? { key: { text: "Date of Death" }, value: { text: result.death_date } } : null
    ];

    return summary.filter(item => item !== null);
  }

}
