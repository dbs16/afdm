import { Injectable, NestMiddleware } from "@nestjs/common";
import { NextFunction, Request, Response } from "express";

@Injectable()
export class ShowPropertyMiddleware implements NestMiddleware {

  use(req: Request, res: Response, next: NextFunction) {
    let showCookie = true;
    let hideBackLink = req.originalUrl === "/";
    // let csrfToken = req.csrfToken();

    try {
      if (req.hasOwnProperty("cookies")) {
        showCookie = req.cookies.hasOwnProperty("cookie_preferences") ? !JSON.parse(req.cookies.cookie_preferences).seen : true;
      }
    } catch (error) {
      showCookie = true;
    }

    res.locals = {
      ...res.locals,
      showCookieBanner: showCookie,
      serviceName: "Search the Armed Forces Memorial Roll of Honour",
      hideBackLink: hideBackLink,
      googleAnalyticsId: process.env.GOOGLE_ANALYTICS_ID || "G-XXXXXXXXX",
      csrfToken: null
    };

    next();
  }

}
