import {
  Controller,
  DefaultValuePipe,
  Get,
  NotFoundException,
  Param,
  ParseIntPipe,
  Post,
  Query,
  Render,
  Req,
  Res,
  Session,
  StreamableFile
} from "@nestjs/common";
import { AppService } from "./app.service";
import { RollOfHonourService } from "./roll_of_honour/roll_of_honour.service";
import { DocumentService } from "./document/document.service";
import { join } from "path";
import * as fs from "fs";


// @UseInterceptors(SessionInterceptor)
@Controller()
export class AppController {
  constructor(private readonly appService: AppService,
              private readonly rollOfHonourService: RollOfHonourService,
              private readonly documentService: DocumentService) {
  }

  @Get()
  index(@Res() res) {
    return res.redirect("/service");
  }


  @Get("/details")
  @Render("details")
  async details(@Req() req, @Res() res, @Session() session) {
    let errors = await req.consumeFlash("errors") ?? [[]];
    const inline = [];
    // Must have a Service in session
    if (!session["service"]) {
      return res.redirect("/");
    }

    if (errors) {
      errors = errors[0];
    }

    if (errors) {
      for (let error of errors) {
        inline[error.href.substring(1)] = { text: error.text };
      }
    }

    return {
      errors: errors,
      inline: inline,
      form: await req.consumeFlash("form") ?? {}
    };
  }

  @Post("/details")
  async storeDetails(@Req() req, @Res() res) {
    const fields = ["firstname", "surname", "service_number"];
    const post = req.body;
    let errors = [];

    if (!post["service_number"] && !post["surname"]) {
      errors.push({
        text: "Enter a service number or last name",
        href: "#service_number"
      });
    } else {
      if (post["surname"] && post["surname"].trim().length < 2) {
        errors.push({
          text: "Enter a last name of at least 2 characters",
          href: "#surname"
        });
      }
    }

    if (errors.length > 0) {
      await req.flash("errors", errors);
      await req.flash("form", post);

      return res.redirect("/details");
    }

    for (let field of fields) {
      if (post.hasOwnProperty(field)) {
        req.session[field] = post[field];
      }
    }

    req.session.save(function() {
      res.redirect("/result");
    });
  }

  @Get("/result")
  async search(@Req() req, @Res() res,
               @Session() session,
               @Query("page", new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
               @Query("limit", new DefaultValuePipe(25), ParseIntPipe) limit: number = 25) {


    if (!session["service"]) {
      return res.redirect("/");
    }

    if (!session["surname"] && !session["service_number"]) {
      return res.redirect("/details");
    }

    let result = await this.rollOfHonourService.search(
      {
        page,
        limit,
        route: "/result",
        service: req.session["service"],
        surname: req.session["surname"],
        forenames: req.session["firstname"],
        service_number: req.session.service_number ?? ""
      });

    const maxPages = Math.ceil(result.meta.totalItems / result.meta.itemsPerPage);

    // Create links for pagination with ellipsis between first and last page
    const links = [];
    const ellipsis = { ellipsis: true };
    for (let i = 1; i <= maxPages; i++) {
      if (i === 1 || i === maxPages || (i >= page - 2 && i <= page + 2)) {
        links.push({ number: i, href: `/result?page=${i}`, current: i === page });
      } else if (i === page - 3 || i === page + 3) {
        links.push(ellipsis);
      }
    }

    result.links["items"] = links;
    if (result.links["previous"]) { // @ts-ignore
      result.links["previous"] = { href: result.links["previous"] };
    }
    if (result.links["next"]) { // @ts-ignore
      result.links["next"] = { href: result.links["next"] };
    }

    return res.render("result", {
      head: [
        { text: "Last name" },
        { text: "First name(s)" },
        { text: "Service" },
        { text: "Rank" },
        { text: "" }
      ],
      rows: result.items.map((row) => {
        return [
          { text: row.surname },
          {
            text: row.forenames.toLowerCase().split(" ")
              .map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(" ")
          },
          { text: row.service },
          { text: row.rank },
          {
            html: `<a href="/serviceperson/${row.id}" class="govuk-link">View</a>`,
            classes: "govuk-table__cell--numeric"
          }
        ];
      }),
      meta: result.meta,
      links: result.links,
      total: result.meta.totalItems
    });
  }

  @Get("/serviceperson/:id")
  @Render("serviceperson")
  async serviceperson(@Req() req, @Res() res, @Param("id", ParseIntPipe) id: number) {
    const result = await this.rollOfHonourService.get(id);

    if (!result) {
      throw new NotFoundException("Serviceperson not found");
    }

    let corpLabel = "Corps";
    switch (result.service) {
      case "Royal Fleet Auxiliary":
      case "Royal Navy":
        corpLabel = "Ship / Unit";
        break;
      case "Royal Air Force":
        corpLabel = "Squadron";
        break;
      case "Army":
        corpLabel = "Regiment";
        break;
      case "Merchant Navy":
        corpLabel = "Regiment";
        break;
    }

    const cemetery_address = result.cemetery_address_1 ? [
      result.cemetery_address_1 ?? null,
      result.cemetery_address_2 ?? null,
      result.cemetery_address_3 ?? null,
      result.cemetery_address_4 ?? null
    ].join("<br>") : null;

    const summary = [
      result.surname ? { key: { text: "Last name" }, value: { text: result.surname } } : null,
      {
        key: { text: "First name(s) / Initial(s)" }, value: {
          text: result.forenames.toLowerCase().split(" ")
            .map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(" ")
        }
      },
      result.rank ? { key: { text: "Rank" }, value: { text: result.rank } } : null,
      result.service ? { key: { text: "Service" }, value: { text: result.service } } : null,
      result.service_no ? { key: { text: "Service Number" }, value: { text: result.service_no } } : null,
      result.regt_corps ? { key: { text: corpLabel }, value: { text: result.regt_corps } } : null,
      result.decorations ? { key: { text: "Decorations" }, value: { text: result.decorations } } : null,
      result.birth_date ? { key: { text: "Date of Birth" }, value: { text: result.birth_date } } : null,
      result.age ? { key: { text: "Age" }, value: { text: result.age } } : null,
      result.death_date ? { key: { text: "Date of Death" }, value: { text: result.death_date } } : null,
      result.cemetery_name ? { key: { text: "Cemetery Name" }, value: { text: result.cemetery_name } } : null,
      cemetery_address ? { key: { text: "Cemetery Address" }, value: { html: cemetery_address } } : null,
      result.grave_section ? { key: { text: "Grave Section" }, value: { text: result.grave_section } } : null,
      result.grave_row ? { key: { text: "Grave Row" }, value: { text: result.grave_row } } : null,
      result.grave_number ? { key: { text: "Grave Number" }, value: { text: result.grave_number } } : null,
      result.memorial ? {
        key: { text: "Included on the Armed Forces Memorial" },
        value: { text: result.memorial?.toLowerCase() === "yes" ? "Yes" : "No" }
      } : null,
      result.nom_roll ? {
        key: { text: "Included on Roll of Honour" },
        value: { text: result.nom_roll?.toLowerCase() === "yes" ? "Yes" : "No" }
      } : null
    ];

    return {
      serviceperson: summary,
      id: id
    };
  }

  @Get("/serviceperson/:id/pdf")
  async download(@Param("id", new ParseIntPipe()) id, @Res() res) {
    const result = await this.rollOfHonourService.get(id);
    const PDFDocument = require("pdfkit-table");
    const SVGtoPDF = require("svg-to-pdfkit");
    const doc = new PDFDocument({ size: "A4", margin: 10 });

    const docWidth = 595.28;
    const docHeight = 841.89;

    PDFDocument.prototype.addSVG = function(svg, x, y, options) {
      return SVGtoPDF(this, svg, x, y, options), this;
    };

    doc.registerFont("Heading Font", join(__dirname, "../fonts/Roboto-Regular.ttf"));
    doc.registerFont("Subheading Font", join(__dirname, "../fonts/Roboto-Regular.ttf"));
    doc.registerFont("Board Font", join(__dirname, "../fonts/Roboto-Regular.ttf"));

    doc.rect(0, 0, docWidth, docHeight).lineWidth(75).stroke("#B99658");
    doc.rect(0, 0, docWidth, docHeight).lineWidth(50).stroke("#305C49");
    doc.rect(0, 0, docWidth, docHeight).lineWidth(50).stroke("#470976");

    const certificate = [
      {
        font: "Subheading Font", size: 28, color: "#470976",
        text: "ARMED FORCES\nMEMORIAL", nextLine: 1
      },
      {
        font: "Subheading Font", size: 28, color: "#470976",
        text: "ROLL OF HONOUR", nextLine: .25
      },
      {
        font: "Subheading Font", size: 16, color: "#B99658",
        text: "TODAY    TOMORROW    FOREVER", nextLine: 1
      }
    ];

    doc.text("", 0, 80);
    doc.text("", 0, 80);
    for (let content of certificate) {
      doc.font(content.font ?? "Helvetica").fontSize(content.size ?? 20)
        .fillColor(content.color ?? "#000000")
        .text(content.text, { width: docWidth, align: "center" })
        .moveDown(content.nextLine ?? .125);
    }

    const data = this.appService.getOutputForRollOfHonour(result, "\n");
    const mapped = data.map((item, index) => {
      return {
        label: { label: item.key.text, width: 150 },
        value: { label: item.value.text?.toString().trim() ?? item.value["html"].toString().trim() }
      };
    });

    doc.table({
      headers: [{ property: "label" }, { property: "value" }],
      datas: mapped
    }, {
      x: 180,
      hideHeader: true,
      width: 300,
      divider: {
        header: { disabled: true },
        horizontal: { disabled: true }
      },
      prepareRow: (row, data) => doc.font("Board Font").fontSize(10).fillColor("#000000")
    });

    doc.addSVG(fs.readFileSync(join(__dirname, "../poppy.svg"), "utf8"), 0, docHeight - 586, {
      // width: 260
    });

    // Send the PDF to the client
    res.setHeader("Content-disposition", "attachment; filename=afdm.pdf");
    res.setHeader("Content-type", "application/pdf");
    doc.pipe(res);
    doc.end();

    // Create a JSON file with date of download
    const date = new Date();
    const dateStr = `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`;
    const json = {
      date: dateStr,
      id: id
    };

    await this.documentService.incrementCounter();

    return new StreamableFile(doc);
  }

  @Get("/search-again")
  searchAgain(@Req() req, @Res() res) {
    req.session.destroy(function() {
      res.redirect("/");
    });
  }

  @Get("/counter")
  async counter() {
    return this.documentService.getCounter();
  }
}
